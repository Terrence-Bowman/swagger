const express = require('express');
const swagger = require('swagger-ui-express');

const swaggerConfig = require('./swagger.json');
const app = express();

const options = {
  customCss: '.swagger-ui .topbar { background: #1C5D77; }'
};

app.use("/docs", swagger.serve, swagger.serve, swagger.setup(swaggerConfig, options));

app.listen(3000, () => console.log('Listening on 3000'));